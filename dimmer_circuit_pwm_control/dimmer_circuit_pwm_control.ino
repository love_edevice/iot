#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* device_name = "dimmer";   // This device is dimmer circuit for lamp.
int upload_interval = 1000;       // Uploading current brightness level and brightness level by potentiometer in miliseconds.

/*    --------  Constant values -----------        */

const char* ssid = "WiFi560-AP";              // RPi3's AP name
const char* password = "Shane_wifi560";       // RPi3's AP password
const char* mqtt_server = "172.24.1.1";   // Server has been built on the router(RPi 3) itself

const char* relay_set_suffix = "/relay_set";       // relay setting for AC220V
//const char* relay_state_suffix = "/relay_state";    // relay state

const char* brightness_level_set_suffix = "/brightness_level/set";   // brightness level of lamp (RPI side)
const char* brightness_poten_level_suffix = "/brightness_poten/level";   // brightness level by potentiometer (Node side)
const char* curr_brightness_level_suffix = "/curr_brightness/level"; // current brightness level

// define pin for relay, power_led, pwm_out
int PIN_relay_switch = 5;  // use D1, GPIO5
int PIN_powerOn_led = 4;   // use D2, GPIO4
int PIN_pwm_out = 14;  // use D5, GPIIO14
#define AnalogPin A0 // define analog pin
//int AnalogPin = 0;

// Global variables
char* curr_brightness_level;
int brightness, brightness_poten, brightness_poten_level;

WiFiClient espClient;
PubSubClient client(espClient);

long lastMsg = 0;
const char* relay_state = "OFF";

char buf_pub_topic[50];
char buf_sub_topic[50];

void setup() {
  pinMode(PIN_relay_switch, OUTPUT);
  pinMode(PIN_pwm_out, OUTPUT);
  digitalWrite(PIN_powerOn_led, LOW);
  pinMode(AnalogPin, INPUT); 

  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  brightness = 0; // set initial brightness
  brightness_poten = 0;
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println("");

  // Check topic of relay setting, and than control powerOn led, relay switch by result.
  set_sub_topic(relay_set_suffix);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

    if (!strncmp((const char*)payload, "ON", 2)) {

      relay_state = "ON";
      Serial.println("Turning RELAY of Dimmer circuit open ON...");
      digitalWrite(PIN_powerOn_led, HIGH);
      digitalWrite(PIN_relay_switch, HIGH);
    }
    else if (!strncmp((const char*)payload, "OFF", 3)) {
      relay_state = "OFF";
      Serial.println("Turning RELAY of Dimmer circuit open OFF...");
      digitalWrite(PIN_powerOn_led, LOW);
      digitalWrite(PIN_relay_switch, LOW);
    }
  }
  // Check topic of brightness level setting, and than control Dimmer circuit by result.
  else {
    set_sub_topic(brightness_level_set_suffix);
    if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

      curr_brightness_level = (char*)payload;

      /*for (int i = 0; i < length; i++) {
        brightness_level[i] = (char)payload[i];
      }*/
      
      int val = atoi(curr_brightness_level);
      SetBrightness(val); 
      if (relay_state == "ON") {
        Serial.println("Brightness will be controled by mobile app...");
        analogWrite(PIN_pwm_out, brightness);
        delay(500);  //????
      }
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("overall_topic", "hello world");
      // ... and resubscribe
      set_sub_topic(relay_set_suffix);
      client.subscribe(buf_sub_topic);
      set_sub_topic(brightness_level_set_suffix);
      client.subscribe(buf_sub_topic);
      client.subscribe("common");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  long now = millis();
  if (now - lastMsg > upload_interval) {
    lastMsg = now;

    brightness_poten = analogRead(AnalogPin);
    brightness_poten_level = (brightness_poten / 100) + 1;
    
    char* buf_brightness_poten_level = new char[10];
    dtostrf(brightness_poten_level, 3, 0, buf_brightness_poten_level);    //???????????

    if ((relay_state == "ON") && (brightness == 0)) {
      analogWrite(PIN_pwm_out, brightness_poten);

      // output current brightness level through serial port
      Serial.print("current brightness_level: ");
      Serial.print(brightness_poten_level);
      Serial.print("( ");
      Serial.print(brightness_poten);
      Serial.println(" )");
    }
    if ((relay_state == "ON") && (brightness != 0)) {
      // output current brightness level through serial port
      Serial.print("current brightness_level: ");
      Serial.print(curr_brightness_level);
      Serial.print("( ");
      Serial.print(brightness);
      Serial.println(" )");
    }
    /*// publish relay state
    set_pub_topic(relay_state_suffix);
    client.publish(buf_pub_topic, relay_state);*/
    
    //publish current brightness level
    set_pub_topic(curr_brightness_level_suffix);
    client.publish(buf_pub_topic, curr_brightness_level);
    // publish brightness level by potentiometer
    set_pub_topic(brightness_poten_level_suffix);
    client.publish(buf_pub_topic, buf_brightness_poten_level);
  }
  else {
    delay(400);   // Loop function takes about 300ms, so 400 ms is enough.
  }
}

void SetBrightness(int val) {    //set brightness value for PWM driver
  switch (val) {
    case 0: brightness = 0; break;
    case 1: brightness = 100; break;
    case 2: brightness = 200; break;
    case 3: brightness = 300; break;
    case 4: brightness = 400; break;
    case 5: brightness = 500; break;
    case 6: brightness = 600; break;
    case 7: brightness = 700; break;
    case 8: brightness = 800; break;
    case 9: brightness = 900; break;
    case 10: brightness = 1000; break;
    default:
      Serial.println("Invalid entry");
  }
  if (brightness > brightness_poten) {  // restrict brightness value for mobile app
    brightness = brightness_poten;
  }
}

void set_pub_topic(const char* suffix) {
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++) {
    if (i < len1)
      buf_pub_topic[i] = device_name[i];
    else
      buf_pub_topic[i] = suffix[i - len1];
  }
  buf_pub_topic[len1 + len2] = '\0';
}

void set_sub_topic(const char* suffix) {
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++) {
    if (i < len1)
      buf_sub_topic[i] = device_name[i];
    else
      buf_sub_topic[i] = suffix[i - len1];
  }
  buf_sub_topic[len1 + len2] = '\0';
}


