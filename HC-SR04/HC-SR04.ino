#define trigPin 13
#define echoPin 12
#define led_red 11
#define led_green 10

void setup() {
  Serial.begin (9600); //select speed of serial interface
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(led_red, OUTPUT);
  pinMode(led_green, OUTPUT); 
}

void loop() {
  long duration, distance;
  digitalWrite(trigPin, LOW);  // Added this line
  delayMicroseconds(2); // Added this line
  digitalWrite(trigPin, HIGH);
//  delayMicroseconds(1000); //- Removed this line
  delayMicroseconds(10); // Added this line
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH); //
  distance = (duration/2) / 29.1;  //
  if (distance < 4) {  // This is where the LED On/Off happens

    /* Next code is wrong for blink operation */
    /*if (led_red == HIGH) {
      delay(100);
      digitalWrite(led_red, LOW);
    }
    else {
      delay(100);
      digitalWrite(led_red, HIGH);
    }
    digitalWrite(led_green, LOW); */
    
    digitalWrite(led_red, HIGH); // When the Red condition is met, the Green LED should turn off
    delay(25);
    digitalWrite(led_red, LOW);
    delay(25);
    digitalWrite(led_green, LOW);  
  }
  else {
    digitalWrite(led_red,LOW);
    digitalWrite(led_green,HIGH);
    delay(100);
    digitalWrite(led_green,LOW);
    delay(100);
  }
  if (distance >= 200 || distance <= 0){
    Serial.println("Out of range");
  }
  else {
    Serial.print(distance);
    Serial.println(" cm");
  }
  delay(500);

}
