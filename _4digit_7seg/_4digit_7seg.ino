#include "SevSeg.h"
#define trigPin 13
#define echoPin 12
SevSeg sevseg; //Instantiate a seven segment object

void setup() {
  Serial.begin (9600); //select speed of serial interface
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
    
  //pinMode(A3, OUTPUT);
  //pinMode(A4, OUTPUT);
  //pinMode(A5, OUTPUT);
  byte numDigits = 4;
  byte digitPins[] = {2, 3, 4, 5};
  byte segmentPins[] = {6, 7, 9, 10, 11, 17, 18, 19};
  sevseg.begin(COMMON_CATHODE, numDigits, digitPins, segmentPins);
  sevseg.setBrightness(90);  
}

void loop() {
  float duration, distance;
  
  digitalWrite(trigPin, LOW);  // Added this line
  delayMicroseconds(2); // Added this line
  digitalWrite(trigPin, HIGH);
  //delayMicroseconds(10); // Added this line
  digitalWrite(trigPin, LOW);
  
  duration = pulseIn(echoPin, HIGH); //
  distance = (duration/2) / 29.1;  //
    
  if (distance >= 200 || distance <= 0){
    Serial.println("Out of range");
    
    sevseg.setNumber(distance, 0);
    sevseg.refreshDisplay(); // Must run repeatedly
  }
  else {
    Serial.print(distance);
    Serial.println(" cm ");
        
    sevseg.setNumber(distance, 0);
    sevseg.refreshDisplay(); // Must run repeatedly
  }
 //delay(10);
}




