#include <TM1637Display.h>
#include <OneWire.h>
#include <DallasTemperature.h>

// define clk and dio of TM1637
#define T1_CLK 12   // use GPIO 12, define clk of TM1637 (current temperature display)
#define T1_DIO 14   // use GPIO 14, define dio of TM1637 (current temperature display)
#define T2_CLK 0    // use GPIO 0, define clk of TM1637 (setting temperature display)    ?????????????????????
#define T2_DIO 2    // use GPIO 2, define dio of TM1637 (setting temperature display)    ?????????????????????

TM1637Display display1(T1_CLK, T1_DIO);
TM1637Display display2(T2_CLK, T2_DIO);

// define data wire of DS18B20
#define ONE_WIRE_BUS 13  // data wire of sensor(DS18B20) is plugged into GPIO13 on the NodeMCU 

OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature DS18B20(&oneWire);

int upload_interval = 1000;   // Uploading current brightness level and brightness level by potentiometer in miliseconds.
long lastMsg = 0;
float DS18B20_temp;

#define TEST_DELAY 850

void setup() {

  Serial.begin(115200);

  //setup pins for TM1637
  //pinMode(T1_CLK, OUTPUT);
  //pinMode(T1_DIO, OUTPUT);
  //pinMode(T2_CLK, OUTPUT);
  //pinMode(T2_DIO, OUTPUT);

  display1.setBrightness(0x0a);
  display2.setBrightness(7, true);

  DS18B20.begin();  // initiation for DS18B20 sensor
  pinMode(ONE_WIRE_BUS, INPUT);

}

void loop() {

  long now = millis();
  if (now - lastMsg > upload_interval) {
    lastMsg = now;

    // Send the command to get temperatures
    DS18B20.requestTemperatures();
    //Why "byIndex"? You can have more than one IC on the same bus.
    DS18B20_temp = DS18B20.getTempCByIndex(0);

    Serial.print("Temperature is: ");
    Serial.println(DS18B20_temp);
  }

  float temp = DS18B20_temp;

  /* This program is that displays integer and negetive integer with decimal point using TM1637 4 digit 7 segment.
     Here have referenced tm1637Display.h and TM1637test.ino files (https://github.com/avishorp/TM1637/blob/master/examples/TM1637Test/TM1637Test.ino) */

  uint8_t data[3];   // define variable for segment digit (This variable contains the value of two digits of temperature and the first digit below decimal point.)

  int v_temp = abs(temp) * 10;
  int temp1 = v_temp / 100;          // 10 digit value of reading value with DS18B20 sensor
  int temp2 = (v_temp % 100) / 10;   // 1 digit value of reading value with DS18B20 sensor
  int temp3 = (v_temp % 100) % 10;   // value for first value below the decimal point

  if (temp < 0) data[0] = 0b01000000; else data[0] = 0b00000000; // if temprature value is negative, displays data[0] as "-" sign.

  if (temp1 != 0) data[1] = display1.encodeDigit(temp1); else data[1] = 0b00000000; // if 1 digit value of temparature value is 0, don't display it.

  switch (temp2) {                          // this part is for displaying the decimal point in 1 digit dot.
    case 0: data[2] = 0b10111111; break;    // MSB bit of data[] is enable bit of dot
    case 1: data[2] = 0b10000110; break;
    case 2: data[2] = 0b11011011; break;
    case 3: data[2] = 0b11001111; break;
    case 4: data[2] = 0b11100110; break;
    case 5: data[2] = 0b11101101; break;
    case 6: data[2] = 0b11111101; break;
    case 7: data[2] = 0b10000111; break;
    case 8: data[2] = 0b11111111; break;
    case 9: data[2] = 0b11101111; break;
  }

  data[3] = display1.encodeDigit(temp3);

  display1.setBrightness(7, true);
  display1.setSegments(data);
  delay(TEST_DELAY);
  display1.setBrightness(7, false);
  display1.setSegments(data);
  delay(TEST_DELAY);

  /*uint8_t data[3];

  data[0] = 0b01000000;
  data[1] = display2.encodeDigit(1);
  data[2] = display2.encodeDigit(2);
  data[3] = display2.encodeDigit(8);

  display2.setBrightness(7, true);
  display2.setSegments(data);
  delay(850);
  display2.setBrightness(7, false);
  display2.setSegments(data);
  delay(850);*/

  /*temp = 566;
  display2.setBrightness(7, true);
  display2.showNumberDecEx(temp, 2, false, 4, 0);

  delay(850);

  display2.setBrightness(7, false);
  display2.showNumberDecEx(temp, 2, false, 4, 0);

  delay(850);*/

}



